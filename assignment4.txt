Jeff Henrichs
10/15/2018
Assignment 4 Questions

1) When recieving a result from another Activity, which method must be
overriden? Explain the meaning of the paramenters to this method

		- onActivityResult(requestCode : Int, resultCode: Int, data: Intent)
		The requestCode is the request id that was sent to the Activity.
		The  resultCode is used to indicate success or cancellation. 
		The data is the content data that was send from the other Activity.
		
2) During which Activity lifecycle method should you save any state in
your application?

		-onPause

3)What is the difference between an explict Intent and an implicit 
Intent?

		-Explict intents directly reference the class of the Activity
		or service you want to start. Implicit initents only specify 
		an action and the Android system will resolve which Activity
		can handle that Intent.

4) Explain what happens when you try to start an Activity using an 
implicit Intent?

		- When you start an Activity using ImplicitIntent, the activity 
		creates an Inent with an action description and passes it to 
		startActivity method. Afterwards, The Android System searches for
		all apps for an intent filter that matches the intent. When a 
		match is found, the system starts the matching activity by 
		invoking its onCreate method and passing it to the Intent.

5) List the Activity lifecycle events in order, for the following 
situation. Activity 1 is already in the created state (onCreate has 
been alreadly called).
		a) A button in Activity 1 is pressed, launching Activity 2
			
		b) Once Activity 2 has become visible, the user presses the 
		back button
		

		c) Activity 1 is displayed and the user presses the back
		button again
		
		
		-for part a the lifecycle events are: 
		onStart(), onResume() and onPause() for Activity 1,
		onStop() and onCreate() for Activity 2.
		
		-for part b the lifecycle events are:
		onStart(), onResume(), onPAuse(), onStop() and onDestory().
		This only affects Activity 2.
		
		-for part c the life cycle events are:
		onRestart(), onStart(), onResume(), onPause(), onStop(), 
		and onDestory().
		This only affects Activity 1
		
		
		
		
		
		