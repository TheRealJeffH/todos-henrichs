package edu.towson.cosc431.henrichs.todos_henrichs

import android.content.Context
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_todo.*
import kotlinx.android.synthetic.main.fragment_todo.view.*


class TodoFragment : Fragment(), IFragmentUpdate {

    override fun showToast(msg : String) {
        Toast.makeText(activity, "${msg} Clicked", Toast.LENGTH_SHORT).show()
    }

    override lateinit var controller: IFragmentController

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_todo, container, false)

        view.all_fragment.setOnClickListener {
            controller.handleAllView()

            showToast("All Fragment")
        }

        view.active_fragment.setOnClickListener {
            controller.handleActiveView()
            showToast("Active Fragment")
        }

        view.completed_fragment.setOnClickListener {
            controller.handleCompletedView()
            showToast("Completed Fragment")
        }

        return view
    }
}
