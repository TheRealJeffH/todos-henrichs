package edu.towson.cosc431.henrichs.todos_henrichs

import android.app.Activity
import android.app.AlertDialog
import android.content.ClipData
import android.content.DialogInterface
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.Toast
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_edit_task.view.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_new_todo.view.*
import kotlinx.android.synthetic.main.layout_listitem.view.*
import java.util.*

class MyAdapter( val taskList: List<Task>, val controller : IController ) : RecyclerView.Adapter<TaskViewHolder>() {

    //number of items in the list
    override fun getItemCount(): Int = taskList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        //how you create a view
        // 1. inflate the view
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_listitem, parent, false)

        // 2. Create and return the viewHolder
        val viewHolder = TaskViewHolder(view)

        viewHolder.itemView.setOnClickListener {
            val position = viewHolder.adapterPosition
            controller.updateTask(position)
            notifyDataSetChanged()
        }

        return viewHolder
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {

        val task = taskList[position]

        holder.itemView.title.text = task.title
        holder.itemView.contents.text = task.contents
        holder.itemView.is_completed_cb.isChecked = task.completed
        holder.itemView.date_created.text  =  task.dateCreated

        val thumbNailImgView = holder.itemView.avatar_cv
        Picasso.get().load(task.image_url).into(thumbNailImgView)
    }

    fun TaskDelete(position: Int){

        controller.deleteTask(taskList[position])
        notifyItemRemoved(position)
    }

    fun TaskCompleted(position: Int){

        controller.completeTask(taskList[position])
        notifyDataSetChanged()
    }

    fun TaskMove(position1: Int, position2: Int): Boolean {
        if (position1 < position2) {
            for (i in position1 until position2){
                Collections.swap(taskList, i, i+1)
            }
        } else {
            for (i in position1 until position2){
                Collections.swap(taskList, i, i-1)
            }
        }
        notifyItemMoved(position1, position2)
        return true
    }

}

class TaskViewHolder(val view: View?): RecyclerView.ViewHolder(view){}