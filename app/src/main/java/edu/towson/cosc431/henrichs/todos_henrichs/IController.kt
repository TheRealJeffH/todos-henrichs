package edu.towson.cosc431.henrichs.todos_henrichs

interface IController {
    fun deleteTask(task: Task)
    fun updateTask(idx: Int)
    fun addTask(task: Task)
    fun completeTask(task: Task)
}

interface IFragmentController {
    fun handleAllView()
    fun handleActiveView()
    fun handleCompletedView()
}

interface IFragmentUpdate {
    fun showToast(msg:String)
    var controller: IFragmentController
}