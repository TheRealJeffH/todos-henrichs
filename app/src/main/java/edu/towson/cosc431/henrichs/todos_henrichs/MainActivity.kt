package edu.towson.cosc431.henrichs.todos_henrichs

import android.annotation.TargetApi
import android.app.*
import android.content.ComponentName
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.ActionMode
import android.widget.Toast
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.*
import java.io.IOException
import java.time.LocalDate
import java.util.*

class MainActivity : AppCompatActivity(), IController, IFragmentController {
    override fun handleAllView() {

        //1. instantiate the adapter
        val thisAdapter = MyAdapter(tasksList, this)


        //2. set the LayoutManager on the recyclerView
        my_recycler_view.layoutManager = LinearLayoutManager(this)

        //3. set the adapter on the recyclerView
        my_recycler_view.adapter = thisAdapter


    }

    override fun handleActiveView() {
        tasksList.forEachIndexed {index, todosDataClass ->  if(!tasksList[index].completed){
            if (!activeList.contains(tasksList[index])) {
                activeList.add(tasksList[index])
            }
        } }
        //1. instantiate the TodosAdapter
        val todosAdapter = MyAdapter(activeList, this)


        //2. set the LayoutManager on the recyclerView
        my_recycler_view.layoutManager = LinearLayoutManager(this)

        //3. set the adapter on the recyclerView
        my_recycler_view.adapter = todosAdapter



    }

    override fun handleCompletedView() {
        tasksList.forEachIndexed {index, todosDataClass ->  if(tasksList[index].completed){
            if (!completedList.contains(tasksList[index])) {
                completedList.add(tasksList[index])
            }
        } }
        //1. instantiate the TodosAdapter
        val thisAdapter = MyAdapter(completedList, this)


        //2. set the LayoutManager on the recyclerView
        my_recycler_view.layoutManager = LinearLayoutManager(this)

        //3. set the adapter on the recyclerView
        my_recycler_view.adapter = thisAdapter

    }

    var tasksList: MutableList<Task> = mutableListOf()
    var activeList: MutableList<Task> = mutableListOf()
    var completedList: MutableList<Task> = mutableListOf()
    var position: Int = 0
    lateinit var todosFragment : IFragmentUpdate
    lateinit var db: IDatabase
    lateinit var notificationManager: NotificationManager
    lateinit var notificationChannel: NotificationChannel
    lateinit var builder : Notification.Builder
    private val channelId = "edu.towson.cosc431.henrichs.todos_henrichs"
    private val description = "Test notification"



    @TargetApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val intent = Intent(this, LauncherActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0 , intent, PendingIntent.FLAG_CANCEL_CURRENT)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel = NotificationChannel(channelId, description, NotificationManager.IMPORTANCE_HIGH)
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.GREEN
            notificationChannel.enableVibration(false)
            notificationManager.createNotificationChannel(notificationChannel)


            builder = Notification.Builder(this, channelId)
                .setContentTitle("Notification")
                .setContentText("You succesfully made an automatic notification")
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setLargeIcon(BitmapFactory.decodeResource(this.resources, R.mipmap.ic_launcher))
                .setContentIntent(pendingIntent)



        }else{
            builder = Notification.Builder(this)
                .setContentTitle("Notification")
                .setContentText("You succesfully made a notification")
                .setSmallIcon(R.drawable.ic_launcher_round)
                .setLargeIcon(BitmapFactory.decodeResource(this.resources, R.drawable.ic_launcher))
                .setContentIntent(pendingIntent)
        }
        notificationManager.notify(2345, builder.build())

        addNewTaskBtn.setOnClickListener { launchSecondActivity() }

        todosFragment = supportFragmentManager.findFragmentById(R.id.fragment) as IFragmentUpdate
        todosFragment.controller = this


        db = TodosDatabase(this)

        /*populateTodoList()*/

        tasksList.addAll(db.getTodos())

        my_recycler_view.layoutManager = LinearLayoutManager(this)

        //help render items in the recycler view
      //  my_recycler_view.adapter = MyAdapter(tasksList, this)

        val itemHelper = TaskItemHelper(TaskCallback(my_recycler_view.adapter as MyAdapter))
        itemHelper.attachToRecyclerView(my_recycler_view)

        fetchJson(this)

    }

    fun fetchJson (controller: IController){
        val myUrl = "https://my-json-server.typicode.com/rvalis-towson/todos_api/todos"

        val request = Request.Builder().url(myUrl).build()

        val client = OkHttpClient()
        client.newCall(request).enqueue(object: Callback {

            override fun onResponse(call: Call, response: Response) {

                val body = response.body()?.string()
                println(body)

                val gson = GsonBuilder().create()

                val listFeed: List<Task> = gson.fromJson(body, Array<Task>::class.java).toList()



                runOnUiThread {
                    my_recycler_view.adapter = MyAdapter(listFeed, controller)
                }
                //val listFeed: List<Task> = gson.fromJson(body, listOf<Task>()::class.java)

            }

            override fun onFailure(call: Call, e: IOException) {
                println("Failed to execute")
            }
        })
    }

    override fun addTask(task: Task){
        db.addTodo(task)
        tasksList.clear()
        tasksList.addAll(db.getTodos())
        my_recycler_view.adapter.notifyDataSetChanged()
    }

    override fun completeTask(task: Task) {

        val completedTask = task.copy(completed = !task.completed)
        db.updateTodo(completedTask)
        tasksList.clear()
        tasksList.addAll(db.getTodos())
        my_recycler_view.adapter.notifyDataSetChanged()
        Toast.makeText(this, "Task Completed", Toast.LENGTH_SHORT).show()
    }


    override fun updateTask(idx: Int) {
        position = idx

        Toast.makeText(this, "Position "+idx+" clicked", Toast.LENGTH_SHORT).show()

        val intent = Intent(this, EditTask::class.java)
        intent.putExtra("key", idx)
        startActivityForResult(intent, EDIT_TASK_REQUEST_CODE)
    }

    override fun deleteTask(task: Task) {
        db.deleteTodo(task)
        tasksList.clear()
        tasksList.addAll(db.getTodos())
        my_recycler_view.adapter.notifyDataSetChanged()
        Toast.makeText(this, "Task Deleted", Toast.LENGTH_SHORT).show()
//        tasksList.removeAt(idx)
    }

    private fun launchSecondActivity(){
        val intent = Intent(this, NewTodoActivity::class.java)
        intent.putExtra("key2", position)
        startActivityForResult(intent, ADD_NEW_TASK_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode){
            ADD_NEW_TASK_REQUEST_CODE -> {
                when(resultCode){
                    Activity.RESULT_OK -> {

                        val json = data?.extras?.get(NewTodoActivity.TASK_EXTRA) as String?
                        if(json != null) {
                            val task = Gson().fromJson<Task>(json, Task::class.java)
                            addTask(task)
                            my_recycler_view.adapter.notifyDataSetChanged()
                        }
                    }
                    Activity.RESULT_CANCELED -> {
                        Toast.makeText(this, "Task Deleted", Toast.LENGTH_SHORT).show()
                    }
                }
            }
            EDIT_TASK_REQUEST_CODE -> {
                when(resultCode){
                    Activity.RESULT_OK -> {
                        val json = data?.extras?.get(EditTask.EDIT_TASK_EXTRA) as String?
                        if(json != null) {
                            val task = Gson().fromJson<Task>(json, Task::class.java)

                            tasksList.set(position, task)
                            my_recycler_view.adapter.notifyDataSetChanged()
                        }
                    }
                    Activity.RESULT_CANCELED -> {
                        Toast.makeText(this, "Task Unchanged", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }

   /* @TargetApi(Build.VERSION_CODES.O)
    private fun populateTodoList() {

        val date = LocalDate.now().toString()  //gets the current day
        (1..10).forEach{
            Task(it, "Title"+it, "Contents"+it, it%2==0, date)
        } //creates 10 dummy tasks
    }*/

    companion object {
        val ADD_NEW_TASK_REQUEST_CODE = 1
        val EDIT_TASK_REQUEST_CODE = 2
    }
}
