package edu.towson.cosc431.henrichs.todos_henrichs

import android.annotation.TargetApi
import android.app.Activity
import android.content.Intent
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_new_todo.*
import kotlinx.android.synthetic.main.layout_listitem.*
import java.time.LocalDate
import java.util.*

class NewTodoActivity : AppCompatActivity() {

    var position: Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_todo)
        position = intent.getIntExtra("key2", -1)
        saveBtn.setOnClickListener { sendTaskInfo() }
    }


    @TargetApi(Build.VERSION_CODES.O)
    private fun sendTaskInfo(){

        val date = LocalDate.now().toString()



        val task = Task(
            position,
            title_et.editableText.toString(),
            contents_et.editableText.toString(),
            iscomplete_cb.isChecked,
            avatar_cv.toString(),
            date,
            false
        )


        if(!task.isValid()){
            Toast.makeText(this, task.getError(), Toast.LENGTH_SHORT).show()
        }else {
            val bundle = Intent()
            bundle.putExtra(TASK_EXTRA, task.toJson())
            setResult(Activity.RESULT_OK, bundle)
            finish()
        }
    }

    companion object {
        val TASK_EXTRA = "task_extra"
    }

}