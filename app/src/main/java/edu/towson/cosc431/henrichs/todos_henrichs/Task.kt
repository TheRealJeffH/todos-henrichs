package edu.towson.cosc431.henrichs.todos_henrichs

import com.google.gson.Gson
import java.util.*

data class Task (   val id: Int,
                    val title : String,
                    val contents: String,
                    val completed : Boolean,
                    val image_url: String,
                    val dateCreated : String,
                    val isDeleted: Boolean = false){



    fun toJson() : String {
        return Gson().toJson(this)
    }

    fun isValid(): Boolean {
        return title.isNotEmpty()
    }

    fun getError(): String {
        if(title.isEmpty()){
            return "Task title is empty"
        }
        throw Exception("Task is valid")
    }
}