package edu.towson.cosc431.henrichs.todos_henrichs

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import java.util.*

interface IDatabase {
    fun getTodos(): List<Task>
    fun getTodo(id: String): List<Task>
    fun addTodo(task: Task)
    fun updateTodo(task: Task)
    fun deleteTodo(task: Task)
    fun activeTodo(): List<Task>
    fun completeTodo(): List<Task>
}

object TaskContract {
    object TaskEntry {
        const val TABLE_NAME = "tasks"
        const val COLUMN_NAME_ID = "tasks_id"
        const val COLUMN_NAME_TITLE = "tasks_title"
        const val COLUMN_NAME_CONTENTS = "tasks_contents"
        const val COLUMN_NAME_COMPLETED = "tasks_completed"
        const val COLUMN_NAME_IMAGE_URL = "tasks_imageUrl"
        const val COLUMN_NAME_DATE = "tasks_date"
        const val COLUMN_NAME_DELETED = "tasks_deleted"
    }
}

private const val SQL_CREATE_ENTRIES = "CREATE TABLE ${TaskContract.TaskEntry.TABLE_NAME} ( " +
        "${TaskContract.TaskEntry.COLUMN_NAME_ID} TEXT PRIMARY KEY, " +
        "${TaskContract.TaskEntry.COLUMN_NAME_TITLE} TEXT, " +
        "${TaskContract.TaskEntry.COLUMN_NAME_CONTENTS} TEXT, " +
        "${TaskContract.TaskEntry.COLUMN_NAME_COMPLETED} INTEGER DEFAULT 0, " +
        "${TaskContract.TaskEntry.COLUMN_NAME_IMAGE_URL} TEXT, "+
        "${TaskContract.TaskEntry.COLUMN_NAME_DATE} TEXT," +
        "${TaskContract.TaskEntry.COLUMN_NAME_DELETED} INTEGER DEFAULT 0 )"

private const val SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS ${TaskContract.TaskEntry.TABLE_NAME}"

class TodosDatabase(ctx: Context) : IDatabase {

    override fun activeTodo(): List<Task> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun completeTodo(): List<Task> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun updateTodo(task: Task) {

        val contentValues = toContentValues(task)

        db.update(
            TaskContract.TaskEntry.TABLE_NAME,
            contentValues,
            "${TaskContract.TaskEntry.COLUMN_NAME_ID} = ?",
            arrayOf(task.id.toString())
        )
    }



    override fun addTodo(task: Task) {

        val contentValues = toContentValues(task)

        db.insert(
            TaskContract.TaskEntry.TABLE_NAME,
            null,
            contentValues
        )
    }

    override fun deleteTodo(task: Task) {

        val deletedTask = task.copy(isDeleted = true)
        val contentValues = toContentValues(deletedTask)

        db.update(
            TaskContract.TaskEntry.TABLE_NAME,
            contentValues,
            "${TaskContract.TaskEntry.COLUMN_NAME_ID} = ?",
            arrayOf(deletedTask.id.toString())
        )
    }

    override fun getTodo(id: String): List<Task> {

        val cursor = db
            .rawQuery(
                "SELECT * FROM ${TaskContract.TaskEntry.TABLE_NAME} " +
                        "WHERE ${TaskContract.TaskEntry.COLUMN_NAME_DELETED}=0 AND ${TaskContract.TaskEntry.COLUMN_NAME_ID} = ${id} "
                , null
            )

        val result = mutableListOf<Task>()

        while(cursor.moveToNext()) {
            val taskid = cursor
                .getInt(
                    cursor.getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_ID)
                )

            val taskTitle = cursor
                .getString(
                    cursor.getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_TITLE)
                )
            val taskContents = cursor
                .getString(
                    cursor.getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_CONTENTS)
                )
            val isCompleteAsInt = cursor
                .getInt(
                    cursor.getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_COMPLETED)
                )
            val taskImgUrl = cursor
                .getString(
                    cursor.getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_IMAGE_URL)
                )
            val date = cursor
                .getString(
                    cursor.getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_DATE)
                )
            val isDeletedAsInt = cursor
                .getInt(
                    cursor.getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_DELETED)
                )
            val task = Task(id = taskid
                , title = taskTitle
                , contents = taskContents
                , completed = isCompleteAsInt == 1
                , image_url = taskImgUrl
                , dateCreated = date
                , isDeleted = isDeletedAsInt == 0
            )
            result.add(task)
        }

        cursor.close()

        return result
    }

    override fun getTodos(): List<Task> {

        val cursor = db
            .rawQuery(
                "SELECT * FROM ${TaskContract.TaskEntry.TABLE_NAME} " +
                        "WHERE ${TaskContract.TaskEntry.COLUMN_NAME_DELETED}=0"
                , null
            )

        val result = mutableListOf<Task>()

        while(cursor.moveToNext()) {
            val taskId = cursor
                .getInt(
                    cursor.getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_ID)
                )

            val taskTitle = cursor
                .getString(
                    cursor.getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_TITLE)
                )
            val taskContents = cursor
                .getString(
                    cursor.getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_CONTENTS)
                )
            val isCompleteAsInt = cursor
                .getInt(
                    cursor.getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_COMPLETED)
                )
            val taskImgUrl = cursor
                .getString(
                    cursor.getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_IMAGE_URL)
                )
            val date = cursor
                .getString(
                    cursor.getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_DATE)
                )
            val isDeletedAsInt = cursor
                .getInt(
                    cursor.getColumnIndex(TaskContract.TaskEntry.COLUMN_NAME_DELETED)
                )
            val task = Task(id = taskId
                    , title = taskTitle
                    , contents = taskContents
                    , completed = isCompleteAsInt == 1
                    , image_url = taskImgUrl
                    , dateCreated = date
                    , isDeleted = isDeletedAsInt == 0
            )
            result.add(task)
        }

        cursor.close()

        return result
    }

    private fun toContentValues(task: Task): ContentValues {
        val cv = ContentValues()

        cv.put(TaskContract.TaskEntry.COLUMN_NAME_ID, task.id)
        cv.put(TaskContract.TaskEntry.COLUMN_NAME_TITLE, task.title)
        cv.put(TaskContract.TaskEntry.COLUMN_NAME_CONTENTS, task.contents)
        val isCompletedAsInt = when(task.completed) {
            true -> 1
            false -> 0
        }
        cv.put(TaskContract.TaskEntry.COLUMN_NAME_COMPLETED, isCompletedAsInt)
        cv.put(TaskContract.TaskEntry.COLUMN_NAME_IMAGE_URL, task.image_url)
        cv.put(TaskContract.TaskEntry.COLUMN_NAME_DATE, task.dateCreated)
        val isDeletedAsInt = when(task.isDeleted) {
            true -> 1
            false -> 0
        }
        cv.put(TaskContract.TaskEntry.COLUMN_NAME_DELETED, isDeletedAsInt)

        return cv
    }

    private val db: SQLiteDatabase = TaskDbHelper(ctx).writableDatabase

    class TaskDbHelper(ctx: Context) : SQLiteOpenHelper(ctx, DATABASE_NAME, null, DATABASE_VERSION){

        override fun onCreate(db: SQLiteDatabase?) {
            db?.execSQL(SQL_CREATE_ENTRIES)
        }

        override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
            db?.execSQL(SQL_DELETE_ENTRIES)
            onCreate(db)
        }

        companion object {
            val DATABASE_NAME = "todos.db"
            val DATABASE_VERSION = 6
        }
    }

}