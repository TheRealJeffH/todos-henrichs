package edu.towson.cosc431.henrichs.todos_henrichs

import android.annotation.TargetApi
import android.app.Activity
import android.content.Intent
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_edit_task.*
import kotlinx.android.synthetic.main.activity_new_todo.*
import kotlinx.android.synthetic.main.layout_listitem.*
import java.time.LocalDate
import java.util.*
import kotlin.collections.RandomAccess


class EditTask : AppCompatActivity() {

    var position: Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_task)
        position =  intent.getIntExtra("key", -1)

        saveButton.setOnClickListener { reviseTaskInfo() }
    }

    @TargetApi(Build.VERSION_CODES.O)
    fun reviseTaskInfo(){
        val date = LocalDate.now().toString()


        val editedTask = Task(
            position,
            title_edit.editableText.toString(),
            contents_edit.editableText.toString(),
            isComplete_cb.isChecked,
            avatar_cv.toString(),
            date,
            false
        )



        if(!editedTask.isValid()){
            Toast.makeText(this, editedTask.getError(), Toast.LENGTH_SHORT).show()
        }else {
            val bundle = Intent()
            bundle.putExtra(EditTask.EDIT_TASK_EXTRA, editedTask.toJson())
            setResult(Activity.RESULT_OK, bundle)
            finish()
        }
    }

    companion object {
        val EDIT_TASK_EXTRA = "edit_task_extra"
    }
}